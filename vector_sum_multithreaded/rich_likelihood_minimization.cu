//-*- c++ -*- 						\
\                                                                                                                                               
\                                                                                                                                                

                                                                                                                         

#include <iostream>
#include <cuda.h>
#include <cuda_runtime.h>
#include <fstream>
#include <string>
#include <sstream>

#include <map>
#include <vector>



#define BLOCK_SIZE 256

using namespace std;



int cpu_tk_id[126];









int n_pixel=13000;

int n_hyp=4;

int n_tracks=128;

int nphotons=20; //max number of photons associated to 1 pixel     

int max_pixel_per_track=1000;

int nevents=31;



__device__ int d_n_pixel=13000;

__device__ int d_n_hyp=4;

__device__ int d_n_tracks=128;

__device__ int d_nphotons=20;

__device__ int d_max_pixel_per_track=1000;


__device__ int d_nevents=31;




__device__ int pid_types[4]={0,1,2,3};




void FillArrayWithMCDataPhotonContainer(double *&photon_sig_container, double *&photon_sig_track){


  //i = (k1 * d2 * d3 * d4) + (k2 * d3 * d4) + (k3 * d4) + k4 4D  



  std::vector<std::vector<int>> track_pion(n_tracks);
  std::vector<std::vector<int>> track_muon(n_tracks);
  std::vector<std::vector<int>> track_kaon(n_tracks);
  std::vector<std::vector<int>> track_proton(n_tracks);
  

  std::vector<std::vector<double>> sig_pion(n_tracks);
  std::vector<std::vector<double>> sig_muon(n_tracks);
  std::vector<std::vector<double>> sig_kaon(n_tracks);
  std::vector<std::vector<double>> sig_proton(n_tracks);
  

  
  int index;

  for(int n=0;n<nevents;n++){


  for(int i=0;i<n_tracks;i++){

    for(int j=0;j<max_pixel_per_track;j++){


      for(int z=0;z<n_hyp;z++){


	//i = (k1 * d2 * d3 * d4) + (k2 * d3 * d4) + (k3 * d4) + k4 

	index=n*n_tracks*max_pixel_per_track*n_hyp+i*max_pixel_per_track*n_hyp+j*n_hyp+z;


	//index= i*max_pixel_per_track*n_hyp+j*n_hyp+z;

	photon_sig_container[index]=0;


	photon_sig_track[index]=-1;


      }




    }


  }

  
  } // loop over events




  
  std::string pixel, signal, hyp, tk_index, phot_index, phot_id;




  std::string line;
  std::ifstream myfile ("likelihood_params_newformat.txt");
  if (myfile.is_open())
    {
      while ( getline (myfile,line) )
        {


	  std::stringstream ss(line);

          ss >> pixel >> signal >> hyp >> tk_index >> phot_index >> phot_id;

	  if(hyp=="pion" && std::stoi(tk_index)< n_tracks){


	    track_pion[std::stoi(tk_index)].push_back(std::stoi(pixel));

	    sig_pion[std::stoi(tk_index)].push_back(std::stod(signal));


	  }



	  
	  if(hyp=="muon" && std::stoi(tk_index)< n_tracks){


            track_muon[std::stoi(tk_index)].push_back(std::stoi(pixel));

	    sig_muon[std::stoi(tk_index)].push_back(std::stod(signal));


          }




	  if(hyp=="kaon" && std::stoi(tk_index)< n_tracks){


            track_kaon[std::stoi(tk_index)].push_back(std::stoi(pixel));

	    sig_kaon[std::stoi(tk_index)].push_back(std::stod(signal));


          }



	  if(hyp=="proton" && std::stoi(tk_index)< n_tracks){


            track_proton[std::stoi(tk_index)].push_back(std::stoi(pixel));

	    sig_proton[std::stoi(tk_index)].push_back(std::stod(signal));


          }





	}




    }



  

  // fill vector

  for(int n=0;n<nevents;n++){


  for(int i=0;i<n_tracks;i++){

    for(int j=0;j<max_pixel_per_track;j++){


      for(int z=0;z<n_hyp;z++){


	index=n*n_tracks*max_pixel_per_track*n_hyp+i*max_pixel_per_track*n_hyp+j*n_hyp+z;

        //index= i*max_pixel_per_track*n_hyp+j*n_hyp+z;



	if(z==0){ //muon

	  if(j<sig_muon[i].size()){
	  

	  photon_sig_container[index]=sig_muon[i].at(j);


	  photon_sig_track[index]=track_muon[i].at(j);

	  }



	}





	if(z==1){ //pion


	  if(j<sig_pion[i].size()){


	    photon_sig_container[index]=sig_pion[i].at(j);


	    photon_sig_track[index]=track_pion[i].at(j);

          }

	  



	}

	

	if(z==2){ //kaon


	  if(j<sig_kaon[i].size()){


	    photon_sig_container[index]=sig_kaon[i].at(j);


	    photon_sig_track[index]=track_kaon[i].at(j);

          }




	}



	
	if(z==3){ //proton


	  if(j<sig_proton[i].size()){


	    photon_sig_container[index]=sig_proton[i].at(j);


	    photon_sig_track[index]=track_proton[i].at(j);

          }





	}








      }




    }


  }



  } // loop over events




  //Print to check

  /*  
  for(int i=0;i<n_tracks;i++){


    cout << " track:" <<i <<" ";


    for(int j=0;j<max_pixel_per_track;j++){


      for(int z=0;z<n_hyp;z++){



	index= i*max_pixel_per_track*n_hyp+j*n_hyp+z;

	
	if(z==1){


	  cout << photon_sig_track[index] << " ";




	}



      }




    }

    cout << endl;





    }*/










  /*   for(int i=0;i<n_tracks;i++){


    cout << "track " << i <<": size " << sig_pion[i].size() <<endl;


     for(int j=0;j<(int)sig_pion[i].size();j++){


      cout << sig_pion[i].at(j) << " ";




      }


      cout << endl;




      }*/









}










void FillArrayWithMCDataTrack(double *&track_sig){


  int index;



  for(int n=0;n<nevents;n++){

  for(int i=0;i<n_tracks;i++){


    for(int j=0;j<n_hyp;j++){

      index=n*n_tracks*n_hyp+i*n_hyp+j;

      //index=i*n_hyp+j;

      track_sig[index]=0;


    }




  }


  }// loop over events



  std::map<std::string, int> pid_mapping;


  pid_mapping.insert(std::make_pair("muon", 0));
  pid_mapping.insert(std::make_pair("pion", 1));
  pid_mapping.insert(std::make_pair("kaon", 2));
  pid_mapping.insert(std::make_pair("proton",3));




  // read track part for the likelihood                                                                                             
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////        

  std::string track, electron, muon, pion, kaon, proton, deuteron, below_threshold;


  std::string line2;


  std::ifstream myfile2 ("likelihood_track_part.txt");
  if (myfile2.is_open())
    {
      while ( getline (myfile2,line2) )
	{


	  std::stringstream ss(line2);

	  ss >> track >> electron >> muon >> pion >> kaon >> proton >> deuteron >> below_threshold;


	  for(int n=0;n<nevents;n++){

	  //index=std::stoi(track)*n_hyp+pid_mapping[hyp];

	    // index=n*n_tracks*n_hyp+i*n_hyp+j;

	    

	  if(std::stoi(track)<n_tracks){



	    //index=std::stoi(track);                                                                                               
	    
	    track_sig[n*n_tracks*n_hyp+std::stoi(track)*n_hyp+pid_mapping["pion"]]=std::stod(pion);

	    //cout << std::stod(pion) << " " << pid_mapping["pion"] << endl;

	    track_sig[n*n_tracks*n_hyp+std::stoi(track)*n_hyp+pid_mapping["proton"]]=std::stod(proton);

	    track_sig[n*n_tracks*n_hyp+std::stoi(track)*n_hyp+pid_mapping["kaon"]]=std::stod(kaon);

	    track_sig[n*n_tracks*n_hyp+std::stoi(track)*n_hyp+pid_mapping["muon"]]=std::stod(muon);



	  }


	  } // loop over events



	}




    }







}







void FillArrayWithMCDataPixel(double * &pixel_sig, int * &pixel_tk, int * &pixel_ph, int *&pid_vec, int *&new_pid, double *&p_sig, double *&log_p_sig, double *&ddl_container){


  //i = (k1 * d2 * d3 * d4 * d5) + (k2 * d3 * d4 * d5) + (k3 * d4 * d5) + (k4 * d5) + k5 5D

  //i = (k1 * d2 * d3 * d4) + (k2 * d3 * d4) + (k3 * d4) + k4 4D  

  //i = (k1 * d2 * d3) + (k2 * d3) + k3 3D


  std::map<std::string, int> pid_mapping;


  pid_mapping.insert(std::make_pair("muon", 0));
  pid_mapping.insert(std::make_pair("pion", 1));
  pid_mapping.insert(std::make_pair("kaon", 2));
  pid_mapping.insert(std::make_pair("proton",3));

  //cout << pid_mapping["electron"] << endl;                                                                                                      


  std::cout << pid_mapping["muon"] << std::endl;



  int index;

  
  for(int n=0;n<nevents;n++){

  
  for(int i=0;i<n_tracks;i++){


    for(int j=0;j<n_hyp;j++){

      index=n*n_tracks*n_hyp+i*n_hyp+j;

      //index=i*n_hyp+j;

      ddl_container[index]=0;



    }
    


  }



  } // loop over events




  for(int n=0;n<nevents;n++){


  for(int i=0;i<n_tracks;i++){


    index=n*n_tracks+i;

    pid_vec[index]=1; //set all to pions initially    
    


  }


  } // loop over events



  for(int n=0;n<nevents;n++){

  for(int i=0;i<n_tracks;i++){


    index=n*n_tracks+i;

    new_pid[index]=2; //set all to kaons                                                                                                    



  }


  } //loop over events


  
  for(int n=0;n<nevents;n++){


  for(int i=0;i<n_pixel;i++){


    index=n*n_tracks+i;

    p_sig[index]=0;

    log_p_sig[index]=0;


  }


  } // loop over events





  for(int n=0;n<nevents;n++){


  for(int i=0;i<n_pixel;i++){


    for(int j=0;j<nphotons;j++){


      index=n*n_pixel*nphotons+i*nphotons+j;

      pixel_ph[index]=-1;

      pixel_tk[index]=-1;


    }




  }



  } // loop over events




  for(int n=0;n<nevents;n++){


  
  for(int i=0;i<n_pixel;i++){

    for(int j=0;j<nphotons;j++){


	for(int z=0;z<n_hyp;z++){


	  
	  index= n*n_pixel*nphotons*n_hyp+i*nphotons*n_hyp+j*n_hyp+z;

	  pixel_sig[index]=0;
  
          


	}




    }


  }



  } // loop over events



 

  double count=0;



  std::string pixel, signal, hyp, tk_index, phot_index, phot_id;




  std::string line;
  std::ifstream myfile ("likelihood_params_newformat.txt");
  if (myfile.is_open())
    {
      while ( getline (myfile,line) )
	{


	  std::stringstream ss(line);

	  ss >> pixel >> signal >> hyp >> tk_index >> phot_index >> phot_id;

	  //cout << pixel <<" " <<  signal <<" " <<  hyp <<" " <<  tk_index <<" " << phot_index <<" " <<  phot_id << endl;

	  for(int n=0;n<nevents;n++){


	    //index=n*n_pixel*nphotons+i*nphotons+j;


	  if(std::stoi(tk_index) < n_tracks && std::stoi(phot_id) < nphotons && std::stoi(pixel) < n_pixel){

	  
	    pixel_ph[n*n_pixel*nphotons+std::stoi(pixel)*nphotons+std::stoi(phot_id)]=std::stoi(phot_index);

	    pixel_tk[n*n_pixel*nphotons+std::stoi(pixel)*nphotons+std::stoi(phot_id)]=std::stoi(tk_index);
	    

	  }


	  //index= n*n_pixel*nphotons*n_hyp+i*nphotons*n_hyp+j*n_hyp+z;

	  //index= i*nphotons*n_tracks*n_hyp+j*n_tracks*n_hyp+z*n_hyp+m;

	  if(std::stoi(pixel) < n_pixel && std::stoi(phot_id) < nphotons && pid_mapping[hyp] < n_hyp){

	  
	    // index=std::stoi(pixel)*nphotons*n_tracks*n_hyp+std::stoi(phot_id)*n_tracks*n_hyp+std::stoi(tk_index)*n_hyp+pid_mapping[hyp];                    
	    index=n*n_pixel*nphotons*n_hyp+std::stoi(pixel)*nphotons*n_hyp+std::stoi(phot_id)*n_hyp+pid_mapping[hyp];


	    pixel_sig[index]=std::stod(signal);


	    if(pid_mapping[hyp]==1){

	      count=count+std::stod(signal);

	    }

	    //pixel_sig[index]=-1.0;

	  //cout << pixel_sig[index] << endl;
	  

	  }



	  } // loop over events




	} // loop over file



    }



  cout << count << endl;

 



}




int linear_index_4D(int k1, int k2, int k3, int k4) {

  // return (k1 * d2 * d3 * d4) + (k2 * d3 * d4) + (k3 * d4) + k4;

  if(k1 < n_pixel && k2 < nphotons && k3 < n_tracks && k4 < n_hyp){

  return  k1*nphotons*n_tracks*n_hyp+k2*n_tracks*n_hyp+k3*n_hyp+k4;


  }

  
  else{ return -1;}




}



int linear_index_2D(int k1, int k2) {

  // return (k1 * d2 * d3) + (k2 * d3) + k3;                                                                                

  if(k1 < n_pixel && k2 < nphotons){

    return  k1*nphotons+k2;


  }


  else{ return -2;}




}




int linear_index_3D(int k1, int k2, int k3) {

  // return (k1 * d2 * d3) + (k2 * d3) + k3;                                                                                                      

  if(k1 < n_pixel && k2 < nphotons && k3 < n_hyp){

    return  k1*nphotons*n_hyp+k2*n_hyp+k3;


  }


  else{ return -3;}




}






__device__ int Get_Track_Id(int k1, int k2, int k3) {

  // return (k1 * d2 * d3) + (k2 * d3) + k3;                                                                                                      

  if(k1 < d_n_pixel && k2 < d_nphotons && k3 < d_nevents){

    return  k3*d_n_pixel*d_nphotons+k1*d_nphotons+k2;


  }


  else{ return -2;}




}











__device__ int GetTrackCurrPID(int track_index, int *track_type_vec, int nevent)
{
  return track_type_vec[nevent*d_n_tracks+track_index];
}









__device__ void logLikelihood(double* d_pixel, double* d_result, int *d_hyp, int *d_tk, double *d_tk_sig, double *d_p_sig, double *d_log_p_sig) {
  int tid = threadIdx.x;

  //int tid = threadIdx.x + 1 * blockDim.x;



    double thread_sum = 0;

    double thread_sum_tk=0;

    double global_sum=0;

    int index;

    int tk_id;

    int curr_pid;
    
    __shared__ double s_partial_sums[BLOCK_SIZE];

    __shared__ double s_partial_sums_tk[BLOCK_SIZE];

    //pixel part

    for(int n=blockIdx.x;n< d_nevents; n+= d_nevents){


    for (int i = tid; i < d_n_pixel; i += blockDim.x) {

      for(int j=0;j<d_nphotons;j++){
	

	tk_id=Get_Track_Id(i,j,n);

	
	if(tk_id>0){

	  if(d_tk[tk_id]>=0){

	
	    curr_pid=GetTrackCurrPID(d_tk[tk_id],d_hyp,n);  


	    //index= i*d_nphotons*d_n_hyp+j*d_n_hyp+curr_pid;


	index=n*d_n_pixel*d_nphotons*d_n_hyp+i*d_nphotons*d_n_hyp+j*d_n_hyp+curr_pid;

	
	if(index<d_n_pixel*d_nphotons*d_n_hyp*d_nevents){

	thread_sum += d_pixel[index];

	}


	}




	} // check if track index > 0
	



      } // loop over y axis (photons)



      //calculate here the function

      d_p_sig[n*d_n_pixel+i]=thread_sum;


      if(thread_sum>=0.001){

	thread_sum=(double)log(exp((double)thread_sum)-1);


      }


      else{

	thread_sum=(double)log(exp((double)0.001)-1);


      }


      d_log_p_sig[n*d_n_pixel+i]=thread_sum;



      global_sum=global_sum+thread_sum;

      thread_sum=0;
     


    } // loop over x axis (hit_pixels)





  





    //  __shared__ double s_partial_sums[BLOCK_SIZE];


    s_partial_sums[tid] = global_sum;
    __syncthreads();

    for (int i = BLOCK_SIZE/2; i > 0; i /= 2) {
        if (tid < i) {
            s_partial_sums[tid] += s_partial_sums[tid + i];
        }
        __syncthreads();
    }

    if (tid == 0) {
        d_result[n] = s_partial_sums[0];
    }



    } // loop over events




    //track part
    
    __syncthreads();


    for(int n=blockIdx.x;n< d_nevents; n+= d_nevents){


    for (int i = tid; i < d_n_tracks; i += blockDim.x) {


      index=n*d_n_tracks*d_n_hyp+i*d_n_hyp+GetTrackCurrPID(i,d_hyp,n);

      //index=i*d_n_hyp+GetTrackCurrPID(i,d_hyp,n);



      if(index<d_n_tracks*d_n_hyp*d_nevents){

        thread_sum_tk += d_tk_sig[index];

      }


    } // loop over tracks


    //__shared__ double s_partial_sums_tk[BLOCK_SIZE];


    s_partial_sums_tk[tid] = thread_sum_tk;
    __syncthreads();



    for (int i = BLOCK_SIZE/2; i > 0; i /= 2) {
      if (tid < i) {
	s_partial_sums_tk[tid] += s_partial_sums_tk[tid + i];
      }
      __syncthreads();
    }

    if (tid == 0) {
      d_result[n] = s_partial_sums_tk[0]-d_result[n];
    }



    } // loop over events


    // --------------------------------------------------------------------                                                                         
    // Compute complete likelihood for event with starting hypotheses                                                                               
    // --------------------------------------------------------------------  











}







__device__ void deltaLogLikelihood(double* d_result, int curr_hypo, int new_hypo,int d_tk, double *d_tk_sig, double *d_p_sig, double *d_log_p_sig, double *d_photon_sig_container, double *d_photon_sig_track,double *d_dll_container){

  int tid = threadIdx.x;

  //i track

  // j pixel

  // z hyp
  
  __shared__ double s_partial_sums[BLOCK_SIZE];

  double thread_sum = 0;

  double temp_ll=d_result[0];


  int index_dll;

  int index_new;

  int index_old;

  int index_new_track;

  int index_old_track;

  int tk=d_tk;

  int new_hyp=new_hypo; //kaon

  int old_hyp=curr_hypo; //pion

  int pixel;

  double pixel_contrib_new;

  double pixel_contrib_old;


  double track_contrib_new;

  double track_contrib_old;


  double test_sig;


  //index= i*max_pixel_per_track*n_hyp+j*n_hyp+z;  


  // loop over the photons for this track

  for(int j=0;j<d_n_tracks;j++){

    thread_sum=0;

    tk=j;


  for (int i = tid; i < d_max_pixel_per_track; i += blockDim.x) {

    index_new=tk*d_max_pixel_per_track*d_n_hyp+i*d_n_hyp+new_hyp;
    
    index_old=tk*d_max_pixel_per_track*d_n_hyp+i*d_n_hyp+old_hyp;


    pixel=d_photon_sig_track[index_new];
    


    if(pixel>=0){

      pixel_contrib_new=d_photon_sig_container[index_new];

      pixel_contrib_old=d_photon_sig_container[index_old];


      test_sig=d_p_sig[pixel]+ pixel_contrib_new - pixel_contrib_old; 


      if(test_sig>=0.001){

        test_sig=(double)log(exp((double)test_sig)-1);


      }


      else{

        test_sig=(double)log(exp((double)0.001)-1);


      }


      thread_sum=thread_sum+(d_log_p_sig[pixel]-test_sig);




    } // if pixel id >=0




  


  } // loop over photons associated to given track



  //__shared__ double s_partial_sums[256];


  s_partial_sums[tid] = thread_sum;

  __syncthreads();

  for (int i = BLOCK_SIZE/2; i > 0; i /= 2) {
    if (tid < i) {
      s_partial_sums[tid] += s_partial_sums[tid + i];
    }
    __syncthreads();
  }

  if (tid == 0) {
    temp_ll = temp_ll+ s_partial_sums[0];
  }



  //track part                                                                                                                                  

  __syncthreads();


  if(tid==0){

    index_dll=tk*d_n_hyp+new_hyp;


    index_new_track=tk*d_n_hyp+new_hyp;

    index_old_track=tk*d_n_hyp+old_hyp; 


    track_contrib_new=d_tk_sig[index_new_track];

    track_contrib_old=d_tk_sig[index_old_track];

    temp_ll = temp_ll+(track_contrib_new-track_contrib_old);

    d_dll_container[index_dll]=d_result[0]-temp_ll;
    
    //d_result[0]=temp_ll;


  }

  __syncthreads();


  }



}









__device__ void deltaLogLikelihood_v2(double* d_result, int *d_curr_hyp, int new_hypo,int d_tk, double *d_tk_sig, double *d_p_sig, double *d_log_p_sig, double *d_photon_sig_container, double *d_photon_sig_track,double *d_dll_container,int event){



  //int tid = threadIdx.x;

  //i track                                                                                                                                       

  // j pixel                                                                                                                                      

  // z hyp                                                                                                                                        

  double thread_sum = 0;

  double temp_ll=d_result[event];


  int index_dll;

  int index_new;

  int index_old;

  int index_new_track;

  int index_old_track;

  int tk=d_tk;

  int new_hyp=new_hypo; //kaon                                                                                                                    

  int old_hyp=d_curr_hyp[event*d_n_tracks+tk]; //pion  


  int pixel;

  double pixel_contrib_new;

  double pixel_contrib_old;


  double track_contrib_new;

  double track_contrib_old;


  double test_sig;



  for (int i = 0; i < d_max_pixel_per_track; i++) {



    index_new= event*d_n_tracks*d_max_pixel_per_track*d_n_hyp+tk*d_max_pixel_per_track*d_n_hyp+i*d_n_hyp+new_hyp;

    index_old= event*d_n_tracks*d_max_pixel_per_track*d_n_hyp+tk*d_max_pixel_per_track*d_n_hyp+i*d_n_hyp+old_hyp;

    //  index_new=tk*d_max_pixel_per_track*d_n_hyp+i*d_n_hyp+new_hyp;

    //index_old=tk*d_max_pixel_per_track*d_n_hyp+i*d_n_hyp+old_hyp;




    pixel=d_photon_sig_track[index_new];


    if(pixel>=0){

      pixel_contrib_new=d_photon_sig_container[index_new];

      pixel_contrib_old=d_photon_sig_container[index_old];


      test_sig=d_p_sig[pixel]+ pixel_contrib_new - pixel_contrib_old;


      if(test_sig>=0.001){

        test_sig=(double)log(exp((double)test_sig)-1);


      }


      else{

        test_sig=(double)log(exp((double)0.001)-1);


      }


      thread_sum=thread_sum+(d_log_p_sig[pixel]-test_sig);




    } // if pixel id >=0                                                                                                                          



  } // loop over photons associated to given track          


  temp_ll = temp_ll+thread_sum;


  //index_dll=tk*d_n_hyp+new_hyp;


  index_dll=event*d_n_tracks*d_n_hyp+tk*d_n_hyp+new_hyp;


  //index_new_track=tk*d_n_hyp+new_hyp;

  //index_old_track=tk*d_n_hyp+old_hyp;


  index_new_track=event*d_n_tracks*d_n_hyp+tk*d_n_hyp+new_hyp;

  index_old_track=event*d_n_tracks*d_n_hyp+tk*d_n_hyp+new_hyp;


  track_contrib_new=d_tk_sig[index_new_track];

  track_contrib_old=d_tk_sig[index_old_track];

  temp_ll = temp_ll+(track_contrib_new-track_contrib_old);

  d_dll_container[index_dll]=-d_result[event]+temp_ll;





}











__device__ void initBestLogLikelihood_v2(double* d_result, int curr_hypo, int new_hypo,int d_tk, double *d_tk_sig, double *d_p_sig, double *d_log_p_sig,double *d_photon_sig_container, double *d_photon_sig_track,double *d_dll_container){

  int tid = threadIdx.x;


  //for (int i = tid; i < d_n_tracks; i += blockDim.x) {


  //for(int j=0;j<d_n_hyp;j++){


      deltaLogLikelihood(d_result,1,2,90,d_tk_sig, d_p_sig, d_log_p_sig, d_photon_sig_container, d_photon_sig_track,d_dll_container);



      //}



      //}




}





__device__ void UpdateTrackIDs(int *d_curr_hyp, double *d_dll_container, double *nchanges){



  int tid = threadIdx.x;


  __shared__ double s_partial_sums[BLOCK_SIZE];

  int index;

  double local_min=0;

  int local_id=-1;

  double thread_sum=0;

  
  //nchanges[0]=0;


  for(int n=blockIdx.x;n< d_nevents; n+= d_nevents){

    nchanges[n]=0;
    

  for (int i = tid; i < d_n_tracks; i += blockDim.x) {


    for(int j=0;j<d_n_hyp;j++){

      index= n*d_n_tracks*d_n_hyp+i*d_n_hyp+j;


      //  index=i*d_n_hyp+j;
      


      if(d_dll_container[index]<local_min){


      local_min=d_dll_container[index];

      local_id=j;

      }

      

    } // loop over PID types


    if(local_id!=-1){
    
      thread_sum=thread_sum+1;


    d_curr_hyp[n*d_n_tracks+i]=local_id;

    }



  } // loop over tracks





  s_partial_sums[tid] = thread_sum;

  __syncthreads();

  for (int i = BLOCK_SIZE/2; i > 0; i /= 2) {
    if (tid < i) {
      s_partial_sums[tid] += s_partial_sums[tid + i];
    }
    __syncthreads();
  }

  if (tid == 0) {
    nchanges[n] =  s_partial_sums[0];
  }



  } // loop over events     






}





__device__ void UpdateOneTrackID(int *d_curr_hyp, double *d_dll_container, double *lm){



  int tid = threadIdx.x;

  int index;
  
  double local_min=0;

  int local_id=-1;

  int local_index;

  int change=0;

  //lm[0]=0;


  for(int n=blockIdx.x;n< d_nevents; n+= d_nevents){


    lm[n]=0;


  if(tid==0){


  for (int i = 0; i < d_n_tracks; i++) {


    for(int j=0;j<d_n_hyp;j++){

      index= n*d_n_tracks*d_n_hyp+i*d_n_hyp+j;

      // index=i*d_n_hyp+j;



      if(d_dll_container[index]<local_min){
      

	local_min=d_dll_container[index];

	local_index=n*d_n_tracks+i;
	
	local_id=j;

	

      }



      if(d_dll_container[index]<0){

	change=1;


      }




    } //loop over PID types


    if(change==1){

      lm[n]=lm[n]+1;

    }
    
    change=0;



  } // loop over tracks

 


  d_curr_hyp[local_index]=local_id;


  }


  } // loop over events



  __syncthreads();


}






__device__ void initBestLogLikelihood(double* d_result, int *d_curr_hyp, double *d_tk_sig, double *d_p_sig, double *d_log_p_sig,double *d_photon_sig_container, double *d_photon_sig_track,double *d_dll_container, double *d_nchanges){

  int tid = threadIdx.x;

  for(int n=blockIdx.x;n< d_nevents; n+= d_nevents){


  for (int i = tid; i < d_n_tracks; i += blockDim.x) {


    for(int j=0;j<d_n_hyp;j++){


      deltaLogLikelihood_v2(d_result,d_curr_hyp,j,i,d_tk_sig, d_p_sig, d_log_p_sig, d_photon_sig_container, d_photon_sig_track,d_dll_container,n);



    }





  }



  } // loop over events




  UpdateTrackIDs(d_curr_hyp,d_dll_container,d_nchanges);




}








__device__ void findBestLogLikelihood(double* d_result, int *d_curr_hyp, double *d_tk_sig, double *d_p_sig, double *d_log_p_sig,double *d_photon_sig_container, double *d_photon_sig_track,double *d_dll_container, double *d_nchanges){

  int tid = threadIdx.x;


  for(int n=blockIdx.x;n< d_nevents; n+= d_nevents){

  for (int i = tid; i < d_n_tracks; i += blockDim.x) {


    for(int j=0;j<d_n_hyp;j++){


      deltaLogLikelihood_v2(d_result,d_curr_hyp,j,i,d_tk_sig, d_p_sig, d_log_p_sig, d_photon_sig_container, d_photon_sig_track,d_dll_container,n);



    }





  }


  } // loop over events



  UpdateOneTrackID(d_curr_hyp,d_dll_container,d_nchanges);




}

















__global__ void SIMDLikelihoodMinimiser(double* d_pixel, double* d_result, int *d_curr_hyp, int *d_new_hyp,int *d_tk, double *d_tk_sig, double *d_p_sig, double *d_log_p_sig, double *d_photon_sig_container, double *d_photon_sig_track,double *d_dll_container, double *d_nchanges){

  //int tid = threadIdx.x;
 
  //int tid = threadIdx.x + blockIdx.x * blockDim.x;

  int tid = threadIdx.x;// + 1 * blockDim.x;  
  


  // calculate initial likelihood starting with all particles being pions hypothesis

  logLikelihood(d_pixel, d_result, d_curr_hyp, d_tk, d_tk_sig, d_p_sig, d_log_p_sig);



  
  initBestLogLikelihood(d_result,d_curr_hyp,d_tk_sig, d_p_sig, d_log_p_sig, d_photon_sig_container, d_photon_sig_track,d_dll_container, d_nchanges);

  

  


  logLikelihood(d_pixel, d_result, d_curr_hyp, d_tk, d_tk_sig, d_p_sig, d_log_p_sig);



  while(d_nchanges[blockIdx.x]>5){


  findBestLogLikelihood(d_result,d_curr_hyp,d_tk_sig, d_p_sig, d_log_p_sig, d_photon_sig_container, d_photon_sig_track,d_dll_container, d_nchanges);

  
  

  logLikelihood(d_pixel, d_result, d_curr_hyp, d_tk, d_tk_sig, d_p_sig, d_log_p_sig);


  }





}








void matrix_sum_wrapper(double* h_pixel, int* h_dims, double* h_result, int dimensions, int *h_hyp, int* h_tk, int * h_ph, double *h_track_sig, int *h_new_hyp, double *h_p_sig, double *h_log_p_sig,double *h_photon_sig_container, double *h_photon_sig_track, double *h_dll_container, double* h_lm) {


  cudaEvent_t start, stop;
  cudaEventCreate(&start);
  cudaEventCreate(&stop);




    int size_x = h_dims[0];  // hit pixels
    int size_y = h_dims[1];  // photons
    int size_z = h_dims[2]; // PID types
    int size_m = h_dims[3]; // ntracks


    int total_size = size_x * size_y*size_z;

    double* d_pixel;
    int* d_dims;
    double* d_result;
    int *d_hyp;
    int *d_new_hyp;
    int * d_tk;
    int *d_ph;
    double *d_track_sig;
    double *d_p_sig;
    double *d_log_p_sig;
    double *d_photon_sig_container; 
    double *d_photon_sig_track;
    double *d_dll_container;
    double *d_lm;



    cudaMalloc((void**)&d_pixel, total_size * nevents* sizeof(double)); // pixel container
    cudaMalloc((void**)&d_dims, dimensions * nevents* sizeof(int));
    cudaMalloc((void**)&d_result, nevents*sizeof(double));
    cudaMalloc((void**)&d_hyp, nevents*n_tracks*sizeof(int)); // vector of PID hyp
    cudaMalloc((void**)&d_new_hyp, nevents*n_tracks*sizeof(int)); // vector of PID hyp
    cudaMalloc((void**)&d_tk, nevents*size_x*size_y*sizeof(int)); // track container
    cudaMalloc((void**)&d_ph, nevents*size_x*size_y*sizeof(int)); // photons container
    cudaMalloc((void**)&d_track_sig, nevents*size_z*size_m*sizeof(double)); // track signal container  
    cudaMalloc((void**)&d_p_sig, nevents*n_pixel*sizeof(double)); // track signal container 
    cudaMalloc((void**)&d_log_p_sig, nevents*n_pixel*sizeof(double)); // track signal container 
    cudaMalloc((void**)&d_photon_sig_container, nevents*n_tracks*n_hyp*max_pixel_per_track*sizeof(double)); // track signal container 
    cudaMalloc((void**)&d_photon_sig_track, nevents*n_tracks*n_hyp*max_pixel_per_track*sizeof(double)); // track signal container 
    cudaMalloc((void**)&d_dll_container, nevents*n_tracks*n_hyp*sizeof(double));
    cudaMalloc((void**)&d_lm, nevents*sizeof(double));



    cudaMemcpy(d_pixel, h_pixel, nevents*total_size * sizeof(double), cudaMemcpyHostToDevice);
    cudaMemcpy(d_dims, h_dims, nevents*dimensions * sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(d_hyp, h_hyp, nevents*n_tracks * sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(d_new_hyp, h_new_hyp, nevents*n_tracks * sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(d_tk, h_tk, nevents*size_x*size_y * sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(d_ph, h_ph, nevents*size_x*size_y * sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(d_track_sig, h_track_sig, nevents*size_z*size_m * sizeof(double), cudaMemcpyHostToDevice);
    cudaMemcpy(d_p_sig, h_track_sig, nevents*n_pixel * sizeof(double), cudaMemcpyHostToDevice);
    cudaMemcpy(d_log_p_sig, h_track_sig, nevents*n_pixel * sizeof(double), cudaMemcpyHostToDevice);
    cudaMemcpy(d_photon_sig_container, h_photon_sig_container, nevents*n_tracks*n_hyp*max_pixel_per_track* sizeof(double), cudaMemcpyHostToDevice);
    cudaMemcpy(d_photon_sig_track, h_photon_sig_track, nevents*n_tracks*n_hyp*max_pixel_per_track * sizeof(double), cudaMemcpyHostToDevice);
    cudaMemcpy(d_dll_container, h_dll_container, nevents*n_tracks*n_hyp * sizeof(double), cudaMemcpyHostToDevice);
    cudaMemcpy(d_lm, h_lm, nevents*sizeof(double), cudaMemcpyHostToDevice);


    cudaEventRecord(start);

    SIMDLikelihoodMinimiser<<<nevents, BLOCK_SIZE>>>(d_pixel, d_result, d_hyp,d_new_hyp, d_tk, d_track_sig, d_p_sig, d_log_p_sig,d_photon_sig_container,d_photon_sig_track,d_dll_container,d_lm);

    cudaEventRecord(stop);

    cudaEventSynchronize(stop);


    float milliseconds = 0;
    cudaEventElapsedTime(&milliseconds, start, stop);

    cout << "ms " << milliseconds << endl;


    cudaMemcpy(h_result, d_result,nevents* sizeof(double), cudaMemcpyDeviceToHost);

    cudaMemcpy(h_lm, d_lm, nevents*sizeof(double), cudaMemcpyDeviceToHost);


    cudaMemcpy(h_dll_container, d_dll_container, nevents*n_tracks*n_hyp * sizeof(double), cudaMemcpyDeviceToHost);

    cudaMemcpy(h_hyp, d_hyp, nevents*n_tracks * sizeof(int), cudaMemcpyDeviceToHost);




    cudaFree(d_pixel);
    cudaFree(d_dims);
    cudaFree(d_result);
    cudaFree(d_hyp);
    cudaFree(d_new_hyp);
    cudaFree(d_tk);
    cudaFree(d_ph);
    cudaFree(d_track_sig);
    cudaFree(d_p_sig);
    cudaFree(d_log_p_sig);
    cudaFree(d_photon_sig_container);
    cudaFree(d_photon_sig_track);
    cudaFree(d_dll_container);
    cudaFree(d_lm);

}

int main() {




  /*
  int n_pixel=13000; // max number of hit pixels

  int n_hyp=4; // track hyp

  int n_tracks=128;// ntracks

  int nphotons=5; //max number of photons associated to 1 pixel  */  




  
  int h_dims[4] = {n_pixel,nphotons,n_hyp,n_tracks};
  

  //int size_evt= nphotons*n_tracks*n_pixel*n_hyp;


  int index;



  double *pixel_container=new double[n_pixel*n_hyp*nphotons*nevents]; //contains the number of photons associated to each pixel for each PID hypothesis

  double *photon_sig_container=new double[n_tracks*n_hyp*max_pixel_per_track*nevents]; // contains the pixels associated to each track for each PID hypothesis

  double *photon_sig_track=new double[n_tracks*n_hyp*max_pixel_per_track*nevents];



  int *tk_container=new int[n_pixel*nphotons*nevents]; // contains the tk IDs associated to each pixel

  int *ph_container=new int[n_pixel*nphotons*nevents]; // contains the photon ID associated to each pixel

  int *initial_pid=new int[n_tracks*nevents]; // initial PID hypothesis

  int *new_pid=new int[n_tracks*nevents]; // new PID hypothesis
  
  double *track_sig_container=new double[n_tracks*n_hyp*nevents]; // contains the contribution from each track for each PID hypothesis


  double *pixel_sig=new double[n_pixel*nevents]; // amount of signal contained in each pixel

  double *log_pixel_sig=new double[n_pixel*nevents]; // log(exp(sig)-1) of the amount of signal contained in each pixel



  double *dll_container=new double[n_tracks*n_hyp*nevents];
  




  // initialize container


  int PIXEL=140;

  int PHOTON_ID=0;

  int PID=0;


  FillArrayWithMCDataPhotonContainer(photon_sig_container,photon_sig_track);



  FillArrayWithMCDataPixel(pixel_container,tk_container,ph_container,initial_pid,new_pid,pixel_sig,log_pixel_sig, dll_container);

  FillArrayWithMCDataTrack(track_sig_container);

  cout << "tk sig " << track_sig_container[0] << endl;

  /*
  if(linear_index_3D(PIXEL,PHOTON_ID,PID)!=-3){


    cout << " pixel signal is " << pixel_container[linear_index_3D(PIXEL,PHOTON_ID,PID)] <<endl;

    cout << "photon id is " << ph_container[linear_index_2D(PIXEL,PHOTON_ID)] <<endl;

    cout << "track id is " << tk_container[linear_index_2D(PIXEL,PHOTON_ID)] <<endl;
    

    

  }


  else{cout << "index out of range" << endl;}*/


  // cout << h_container[0]<<endl;
  
  //pixel, photon, track, pid

  //  cout << linear_index_4D(141,0,50,0) << endl;


  //cout << h_container[linear_index_4D(142,0,93,1)] <<endl;


  double h_result[nevents];

  double h_lm[nevents];
   

  matrix_sum_wrapper(pixel_container, h_dims, h_result, 4,initial_pid, tk_container, ph_container,track_sig_container, new_pid, pixel_sig, log_pixel_sig,photon_sig_container,photon_sig_track,dll_container,h_lm);



  for(int n=0;n<nevents;n++){
  

    cout << "event " << n << endl;

    printf("The sum of the matrix is %f\n", h_result[n]);

    cout << "n changed track is " << h_lm[0] << endl;

  }


  

  //cout << "lm is " << h_lm[0] << endl;
  
    


      for(int i=0;i<n_tracks;i++){


      for(int j=0;j<n_hyp;j++){


	index=i*n_hyp+j;

	cout << " track "<<i <<", hyp "<< j << " " << dll_container[index] << endl;


	//cout<< i << " " << dll_container[index] << endl;  


      }



      }




    
    for(int i=0;i<n_tracks;i++){


      //cout << " track " <<i <<", hyp " << initial_pid[i] << endl;

      cout <<i<< " " << initial_pid[i] << endl;


    }








    return 0;

  


}
